import numpy as np
import torch
from tqdm import tqdm_notebook as tqdm

def train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, change_lr=None,experiment=None):
  with experiment.train():
      for epoch in tqdm(range(1,epochs+1)):
        model.train()
        batch_losses=[]
        trace_y = []
        trace_yhat = []
        if change_lr:
          optimizer = change_lr(optimizer, epoch)
        for i, data in enumerate(train_loader):
          x, y = data
          optimizer.zero_grad()
          x = x.to(device, dtype=torch.float32)
          y = y.to(device, dtype=torch.long)
          y_hat = model(x)
          trace_y.append(y.cpu().detach().numpy())
          trace_yhat.append(y_hat.cpu().detach().numpy())   
          loss = loss_fn(y_hat, y)
          loss.backward()
          batch_losses.append(loss.item())
          optimizer.step()
        trace_y = np.concatenate(trace_y)
        trace_yhat = np.concatenate(trace_yhat)
        accuracy = np.mean(trace_yhat.argmax(axis=1)==trace_y)
        experiment.log_metric("loss",np.mean(batch_losses), epoch=epoch)
        experiment.log_metric("accuracy",accuracy, epoch=epoch)
        print(f'Epoch - {epoch} Train-Loss : {np.mean(batch_losses)} Train-Accuracy : {accuracy}')
        with experiment.validate():
            model.eval()
            batch_losses=[]
            trace_y = []
            trace_yhat = []
            for i, data in enumerate(valid_loader):
              x, y = data
              x = x.to(device, dtype=torch.float32)
              y = y.to(device, dtype=torch.long)
              y_hat = model(x)
              loss = loss_fn(y_hat, y)
              trace_y.append(y.cpu().detach().numpy())
              trace_yhat.append(y_hat.cpu().detach().numpy())      
              batch_losses.append(loss.item())
            trace_y = np.concatenate(trace_y)
            trace_yhat = np.concatenate(trace_yhat)
            accuracy = np.mean(trace_yhat.argmax(axis=1)==trace_y)
            experiment.log_metric("loss",np.mean(batch_losses), epoch=epoch)
            experiment.log_metric("accuracy",accuracy, epoch=epoch)
            print(f'Epoch - {epoch} Valid-Loss : {np.mean(batch_losses)} Valid-Accuracy : {accuracy}')


def setlr(optimizer, lr):
  for param_group in optimizer.param_groups:
    param_group['lr'] = lr
  return optimizer

def lr_decay(optimizer, epoch):
  for param_group in optimizer.param_groups:
    learning_rate = param_group['lr']
  if epoch%40==0:
    new_lr = learning_rate / (10**(epoch//10))
    optimizer = setlr(optimizer, new_lr)
    print(f'Changed learning rate to {new_lr}')
  return optimizer
