from comet_ml import Experiment

import os
import argparse

import numpy as np
np.random.seed(0)

import torch
torch.manual_seed(0)

from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn

from Models import simpleCNN, simpleLSTM, simpleSDT
from datasets import UCI_HAR
from train import train, lr_decay
import trainSDT


def main(hyper_params):

    num_classes = 50
    foldNumber =1
    epochs = hyper_params['epochs']
    batch_size = hyper_params['batch_size']
    learning_rate = hyper_params['learning_rate']
    
    if(len(hyper_params['api_key'])>0):
        # Create an experiment with your api key:
        experiment = Experiment(
            api_key=hyper_params['api_key'],
            project_name="audiofed",
            workspace="amirbalef",
        )
    else:
        experiment =  Experiment(api_key="dummy", disabled=True)
    experiment.log_parameters(hyper_params)
    
    train_data =UCI_HAR('train')
    valid_data =UCI_HAR('valid')
    train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
    valid_loader = DataLoader(valid_data, batch_size=batch_size, shuffle=True)

    if torch.cuda.is_available():
      device=torch.device('cuda:0')
    else:
      device=torch.device('cpu')

    if hyper_params['model']== 'simpleCNN':
        model = simpleCNN.SimpleNetwork()
    if hyper_params['model']== 'simpleLSTM':
        model = simpleLSTM.LSTMModel(n_input=9, n_hidden=256, n_layers=3, n_classes=6, drop_prob=0.5,batch_size=batch_size)
    if hyper_params['model']== 'simpleSDT':
        # Parameters
        input_dim = 128 * 9
        output_dim = 6
        depth = 5
        lamda = 1e-3
        # Model and Optimizer
        model = simpleSDT.SDT(input_dim, output_dim, depth, lamda)

    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    loss_fn = nn.CrossEntropyLoss()
    
    if hyper_params['model']== 'simpleSDT':
        trainSDT.train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, lr_decay, experiment) 
    else:
        train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, lr_decay, experiment)    
    
    #with open('esc50resnet.pth','wb') as f:
    #    torch.save(model, f)
    


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--api_key", type=str, default="")
    parser.add_argument("--dataset", type=str, default="UCI_HAR", choices=["UCI_HAR"])
    parser.add_argument("--model", type=str, default="simpleCNN", choices=["simpleCNN", "simpleLSTM", "simpleSDT"])
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--learning_rate", type=float, default=2e-4, help="learning rate")
    parser.add_argument("--epochs", type=int, default=20)
    parser.add_argument("--optimizer", type=str, default="ADAM",choices=["ADAM"])
    args = parser.parse_args()

    print("=" * 80)
    print("Summary of training process:")
    print("Batch size: {}".format(args.batch_size))
    print("Learing rate       : {}".format(args.learning_rate))
    print("Number of epochs       : {}".format(args.epochs))
    print("Dataset       : {}".format(args.dataset))
    print("Local Model       : {}".format(args.model))
    print("=" * 80)

    hyper_params = {
        "api_key": args.api_key,
        "dataset": args.dataset,
        "model": args.model,
        "batch_size": args.batch_size,
        "learning_rate": args.learning_rate,
        "epochs": args.epochs,
        "optimizer": args.optimizer
    }
    main(hyper_params)
