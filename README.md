
# How to run in Google Colab

Open Google Colab and run following commands:


```python
!pip install comet-ml -q
!git clone https://gitlab.com/amirbalef/har-smartphones.git
```

    [K     |████████████████████████████████| 245kB 12.9MB/s 
    [K     |████████████████████████████████| 512kB 20.2MB/s 
    [K     |████████████████████████████████| 204kB 47.2MB/s 
    [?25h  Building wheel for configobj (setup.py) ... [?25l[?25hdone
    Cloning into 'har-smartphones'...
    remote: Enumerating objects: 37, done.[K
    remote: Counting objects: 100% (37/37), done.[K
    remote: Compressing objects: 100% (37/37), done.[K
    remote: Total 37 (delta 12), reused 0 (delta 0), pack-reused 0[K
    Unpacking objects: 100% (37/37), done.
    

To dowanload dataset:


```python
!wget -q https://archive.ics.uci.edu/ml/machine-learning-databases/00240/UCI%20HAR%20Dataset.zip
!unzip -q "UCI HAR Dataset.zip" -d datasets
%cd har-smartphones
```

To run expriments:


```python
!python3 main.py --epochs 80 --model simpleCNN
```

    ================================================================================
    Summary of training process:
    Batch size: 32
    Learing rate       : 0.0002
    Number of epochs       : 80
    Dataset       : UCI_HAR
    Local Model       : simpleCNN
    ================================================================================
    HBox(children=(FloatProgress(value=0.0, max=80.0), HTML(value='')))
    Epoch - 1 Train-Loss : 1.378944937042568 Train-Accuracy : 0.6779107725788901
    Epoch - 1 Valid-Loss : 1.3258613450552827 Valid-Accuracy : 0.7125890736342043
    Epoch - 2 Train-Loss : 1.2336188202318938 Train-Accuracy : 0.8120239390642002
    Epoch - 2 Valid-Loss : 1.2931091887976534 Valid-Accuracy : 0.7475398710553105
    Epoch - 3 Train-Loss : 1.1299574354420538 Train-Accuracy : 0.9170293797606094
    Epoch - 3 Valid-Loss : 1.1829471062588435 Valid-Accuracy : 0.8642687478791992
    Epoch - 4 Train-Loss : 1.1048094474751016 Train-Accuracy : 0.941512513601741
    Epoch - 4 Valid-Loss : 1.164889636860099 Valid-Accuracy : 0.8812351543942993
    Epoch - 5 Train-Loss : 1.0950147665065268 Train-Accuracy : 0.9498095756256801
    Epoch - 5 Valid-Loss : 1.1607842855556036 Valid-Accuracy : 0.8822531387852053
    Epoch - 6 Train-Loss : 1.0982655701429949 Train-Accuracy : 0.9465451577801959
    Epoch - 6 Valid-Loss : 1.1493743183792278 Valid-Accuracy : 0.8954869358669834
    Epoch - 7 Train-Loss : 1.0938800692558288 Train-Accuracy : 0.9508977149075082
    Epoch - 7 Valid-Loss : 1.156147503083752 Valid-Accuracy : 0.8880217170003394
    Epoch - 8 Train-Loss : 1.090908768384353 Train-Accuracy : 0.9532100108813928
    Epoch - 8 Valid-Loss : 1.1490806020716184 Valid-Accuracy : 0.8985408890397014
    Epoch - 9 Train-Loss : 1.0890613348587699 Train-Accuracy : 0.9552502720348205
    Epoch - 9 Valid-Loss : 1.153819182867645 Valid-Accuracy : 0.8920936545639634
    Epoch - 10 Train-Loss : 1.0894804602083952 Train-Accuracy : 0.9538900979325353
    Epoch - 10 Valid-Loss : 1.1469044454636113 Valid-Accuracy : 0.8954869358669834
    Epoch - 11 Train-Loss : 1.089773943113244 Train-Accuracy : 0.9534820457018498
    Epoch - 11 Valid-Loss : 1.1505159703634118 Valid-Accuracy : 0.8924329826942654
    Epoch - 12 Train-Loss : 1.0906800762466762 Train-Accuracy : 0.9522578890097932
    Epoch - 12 Valid-Loss : 1.1415074409977082 Valid-Accuracy : 0.9012555140821175
    Epoch - 13 Train-Loss : 1.0878954648971557 Train-Accuracy : 0.955930359085963
    Epoch - 13 Valid-Loss : 1.1397812763849895 Valid-Accuracy : 0.9022734984730234
    Epoch - 14 Train-Loss : 1.0866960551427758 Train-Accuracy : 0.9571545157780196
    Epoch - 14 Valid-Loss : 1.1424020016065208 Valid-Accuracy : 0.9002375296912114
    Epoch - 15 Train-Loss : 1.0872991375301195 Train-Accuracy : 0.9563384113166485
    Epoch - 15 Valid-Loss : 1.1416763874792284 Valid-Accuracy : 0.9002375296912114
    Epoch - 16 Train-Loss : 1.0869576195011967 Train-Accuracy : 0.956474428726877
    Epoch - 16 Valid-Loss : 1.14811703594782 Valid-Accuracy : 0.8954869358669834
    Epoch - 17 Train-Loss : 1.0896189300910286 Train-Accuracy : 0.9541621327529923
    Epoch - 17 Valid-Loss : 1.171311915561717 Valid-Accuracy : 0.8710553104852392
    Epoch - 18 Train-Loss : 1.091601390423982 Train-Accuracy : 0.9523939064200218
    Epoch - 18 Valid-Loss : 1.1668395521820232 Valid-Accuracy : 0.8791991856124872
    Epoch - 19 Train-Loss : 1.092773637564286 Train-Accuracy : 0.9508977149075082
    Epoch - 19 Valid-Loss : 1.1764637898373347 Valid-Accuracy : 0.8656260604004072
    Epoch - 20 Train-Loss : 1.094181901993959 Train-Accuracy : 0.9488574537540805
    Epoch - 20 Valid-Loss : 1.1409860849380493 Valid-Accuracy : 0.9005768578215134
    Epoch - 21 Train-Loss : 1.087409444477247 Train-Accuracy : 0.9563384113166485
    Epoch - 21 Valid-Loss : 1.1420752425347604 Valid-Accuracy : 0.9029521547336274
    Epoch - 22 Train-Loss : 1.0857675013334855 Train-Accuracy : 0.9579706202393906
    Epoch - 22 Valid-Loss : 1.1336958869811027 Valid-Accuracy : 0.9090600610790635
    Epoch - 23 Train-Loss : 1.0869053291237873 Train-Accuracy : 0.9568824809575626
    Epoch - 23 Valid-Loss : 1.1340128773002214 Valid-Accuracy : 0.9087207329487614
    Epoch - 24 Train-Loss : 1.0852100450059643 Train-Accuracy : 0.9585146898803046
    Epoch - 24 Valid-Loss : 1.1387250295249365 Valid-Accuracy : 0.9032914828639295
    Epoch - 25 Train-Loss : 1.0865781794423643 Train-Accuracy : 0.9567464635473341
    Epoch - 25 Valid-Loss : 1.1374614110556982 Valid-Accuracy : 0.9043094672548354
    Epoch - 26 Train-Loss : 1.0857492493546528 Train-Accuracy : 0.9576985854189336
    Epoch - 26 Valid-Loss : 1.1380558347189298 Valid-Accuracy : 0.9077027485578555
    Epoch - 27 Train-Loss : 1.0847653274950775 Train-Accuracy : 0.9587867247007617
    Epoch - 27 Valid-Loss : 1.1375812407462829 Valid-Accuracy : 0.9043094672548354
    Epoch - 28 Train-Loss : 1.0891886975454248 Train-Accuracy : 0.954570184983678
    Epoch - 28 Valid-Loss : 1.138292416449516 Valid-Accuracy : 0.9036308109942314
    Epoch - 29 Train-Loss : 1.0894359070321789 Train-Accuracy : 0.9542981501632208
    Epoch - 29 Valid-Loss : 1.1994080658881896 Valid-Accuracy : 0.841533763148965
    Epoch - 30 Train-Loss : 1.0906974336375361 Train-Accuracy : 0.9525299238302503
    Epoch - 30 Valid-Loss : 1.1405327422644502 Valid-Accuracy : 0.9005768578215134
    Epoch - 31 Train-Loss : 1.0849348747211953 Train-Accuracy : 0.9587867247007617
    Epoch - 31 Valid-Loss : 1.1399609247843425 Valid-Accuracy : 0.9063454360366474
    Epoch - 32 Train-Loss : 1.0861357123955437 Train-Accuracy : 0.9572905331882481
    Epoch - 32 Valid-Loss : 1.1423270535725418 Valid-Accuracy : 0.8998982015609094
    Epoch - 33 Train-Loss : 1.0860268359598906 Train-Accuracy : 0.9572905331882481
    Epoch - 33 Valid-Loss : 1.1404388655898392 Valid-Accuracy : 0.9015948422124194
    Epoch - 34 Train-Loss : 1.0836991983911266 Train-Accuracy : 0.9597388465723613
    Epoch - 34 Valid-Loss : 1.137384069863186 Valid-Accuracy : 0.9049881235154394
    Epoch - 35 Train-Loss : 1.0848776537439098 Train-Accuracy : 0.9586507072905331
    Epoch - 35 Valid-Loss : 1.1424224120314403 Valid-Accuracy : 0.9026128266033254
    Epoch - 36 Train-Loss : 1.0870977059654567 Train-Accuracy : 0.956474428726877
    Epoch - 36 Valid-Loss : 1.1429127621394333 Valid-Accuracy : 0.8995588734306074
    Epoch - 37 Train-Loss : 1.0841170238411946 Train-Accuracy : 0.9594668117519043
    Epoch - 37 Valid-Loss : 1.1403031951637679 Valid-Accuracy : 0.9022734984730234
    Epoch - 38 Train-Loss : 1.0849464032960974 Train-Accuracy : 0.9582426550598476
    Epoch - 38 Valid-Loss : 1.1499062686838128 Valid-Accuracy : 0.8920936545639634
    Epoch - 39 Train-Loss : 1.0939649519713028 Train-Accuracy : 0.9499455930359086
    Epoch - 39 Valid-Loss : 1.1460800350353282 Valid-Accuracy : 0.8998982015609094
    Changed learning rate to 2e-08
    Epoch - 40 Train-Loss : 1.0946717780569326 Train-Accuracy : 0.948993471164309
    Epoch - 40 Valid-Loss : 1.1428266109958771 Valid-Accuracy : 0.8998982015609094
    Epoch - 41 Train-Loss : 1.0946632768796838 Train-Accuracy : 0.948993471164309
    Epoch - 41 Valid-Loss : 1.1428223681706253 Valid-Accuracy : 0.8998982015609094
    Epoch - 42 Train-Loss : 1.0947451368622159 Train-Accuracy : 0.948993471164309
    Epoch - 42 Valid-Loss : 1.1428169088978921 Valid-Accuracy : 0.8998982015609094
    Epoch - 43 Train-Loss : 1.0946515425391818 Train-Accuracy : 0.948993471164309
    Epoch - 43 Valid-Loss : 1.1459192716947166 Valid-Accuracy : 0.8998982015609094
    Epoch - 44 Train-Loss : 1.0946420389672984 Train-Accuracy : 0.948993471164309
    Epoch - 44 Valid-Loss : 1.1428074067638767 Valid-Accuracy : 0.8998982015609094
    Epoch - 45 Train-Loss : 1.0946740280026974 Train-Accuracy : 0.948993471164309
    Epoch - 45 Valid-Loss : 1.1428031203567341 Valid-Accuracy : 0.8998982015609094
    Epoch - 46 Train-Loss : 1.0947279240774073 Train-Accuracy : 0.948993471164309
    Epoch - 46 Valid-Loss : 1.1427970958012406 Valid-Accuracy : 0.8998982015609094
    Epoch - 47 Train-Loss : 1.094611508431642 Train-Accuracy : 0.948993471164309
    Epoch - 47 Valid-Loss : 1.1460412048524427 Valid-Accuracy : 0.9002375296912114
    Epoch - 48 Train-Loss : 1.0946028595385344 Train-Accuracy : 0.948993471164309
    Epoch - 48 Valid-Loss : 1.1460343060954925 Valid-Accuracy : 0.9005768578215134
    Epoch - 49 Train-Loss : 1.0945941883584727 Train-Accuracy : 0.948993471164309
    Epoch - 49 Valid-Loss : 1.1427808346286896 Valid-Accuracy : 0.9005768578215134
    Epoch - 50 Train-Loss : 1.0946182178414385 Train-Accuracy : 0.948993471164309
    Epoch - 50 Valid-Loss : 1.142774498590859 Valid-Accuracy : 0.9005768578215134
    Epoch - 51 Train-Loss : 1.094621759393941 Train-Accuracy : 0.948993471164309
    Epoch - 51 Valid-Loss : 1.1427690790545555 Valid-Accuracy : 0.9005768578215134
    Epoch - 52 Train-Loss : 1.094661780025648 Train-Accuracy : 0.948993471164309
    Epoch - 52 Valid-Loss : 1.1460029976342314 Valid-Accuracy : 0.9005768578215134
    Epoch - 53 Train-Loss : 1.0946493744850159 Train-Accuracy : 0.948993471164309
    Epoch - 53 Valid-Loss : 1.1427573965441795 Valid-Accuracy : 0.9005768578215134
    Epoch - 54 Train-Loss : 1.094549862716509 Train-Accuracy : 0.948993471164309
    Epoch - 54 Valid-Loss : 1.1459982933536652 Valid-Accuracy : 0.9005768578215134
    Epoch - 55 Train-Loss : 1.0946767879569013 Train-Accuracy : 0.948993471164309
    Epoch - 55 Valid-Loss : 1.1427459511705624 Valid-Accuracy : 0.9005768578215134
    Epoch - 56 Train-Loss : 1.0946136355400085 Train-Accuracy : 0.948993471164309
    Epoch - 56 Valid-Loss : 1.142738934486143 Valid-Accuracy : 0.9005768578215134
    Epoch - 57 Train-Loss : 1.0945686692776888 Train-Accuracy : 0.948993471164309
    Epoch - 57 Valid-Loss : 1.1427324254025695 Valid-Accuracy : 0.9005768578215134
    Epoch - 58 Train-Loss : 1.0945144337156545 Train-Accuracy : 0.948993471164309
    Epoch - 58 Valid-Loss : 1.1460028117702854 Valid-Accuracy : 0.9005768578215134
    Epoch - 59 Train-Loss : 1.0946413729501807 Train-Accuracy : 0.948993471164309
    Epoch - 59 Valid-Loss : 1.1427208108286704 Valid-Accuracy : 0.9005768578215134
    Epoch - 60 Train-Loss : 1.0945928501046223 Train-Accuracy : 0.948993471164309
    Epoch - 60 Valid-Loss : 1.1459631253314275 Valid-Accuracy : 0.9005768578215134
    Epoch - 61 Train-Loss : 1.0944883579793183 Train-Accuracy : 0.948993471164309
    Epoch - 61 Valid-Loss : 1.1427090437181535 Valid-Accuracy : 0.9005768578215134
    Epoch - 62 Train-Loss : 1.094524796112724 Train-Accuracy : 0.948993471164309
    Epoch - 62 Valid-Loss : 1.1459453387926983 Valid-Accuracy : 0.9005768578215134
    Epoch - 63 Train-Loss : 1.0945174668146216 Train-Accuracy : 0.948993471164309
    Epoch - 63 Valid-Loss : 1.1426975022080124 Valid-Accuracy : 0.9009161859518154
    Epoch - 64 Train-Loss : 1.094598294341046 Train-Accuracy : 0.948993471164309
    Epoch - 64 Valid-Loss : 1.1426911559156192 Valid-Accuracy : 0.9009161859518154
    Epoch - 65 Train-Loss : 1.094454128327577 Train-Accuracy : 0.948993471164309
    Epoch - 65 Valid-Loss : 1.1426852274966497 Valid-Accuracy : 0.9009161859518154
    Epoch - 66 Train-Loss : 1.094445498611616 Train-Accuracy : 0.948993471164309
    Epoch - 66 Valid-Loss : 1.142679145259242 Valid-Accuracy : 0.9009161859518154
    Epoch - 67 Train-Loss : 1.094527546737505 Train-Accuracy : 0.948993471164309
    Epoch - 67 Valid-Loss : 1.1426732668312647 Valid-Accuracy : 0.9009161859518154
    Epoch - 68 Train-Loss : 1.094473678132762 Train-Accuracy : 0.948993471164309
    Epoch - 68 Valid-Loss : 1.1426675588853898 Valid-Accuracy : 0.9009161859518154
    Epoch - 69 Train-Loss : 1.0944653070491293 Train-Accuracy : 0.948993471164309
    Epoch - 69 Valid-Loss : 1.1426622047219226 Valid-Accuracy : 0.9005768578215134
    Epoch - 70 Train-Loss : 1.0945474894150444 Train-Accuracy : 0.948993471164309
    Epoch - 70 Valid-Loss : 1.14265699027687 Valid-Accuracy : 0.9005768578215134
    Epoch - 71 Train-Loss : 1.0944030994954317 Train-Accuracy : 0.948993471164309
    Epoch - 71 Valid-Loss : 1.1458980550048172 Valid-Accuracy : 0.9005768578215134
    Epoch - 72 Train-Loss : 1.094440119681151 Train-Accuracy : 0.948993471164309
    Epoch - 72 Valid-Loss : 1.1426451244661886 Valid-Accuracy : 0.9005768578215134
    Epoch - 73 Train-Loss : 1.0944766008335611 Train-Accuracy : 0.948993471164309
    Epoch - 73 Valid-Loss : 1.1426397280026508 Valid-Accuracy : 0.9005768578215134
    Epoch - 74 Train-Loss : 1.094559100918148 Train-Accuracy : 0.948993471164309
    Epoch - 74 Valid-Loss : 1.1426342084843626 Valid-Accuracy : 0.9002375296912114
    Epoch - 75 Train-Loss : 1.0943697037904159 Train-Accuracy : 0.948993471164309
    Epoch - 75 Valid-Loss : 1.1426313641250774 Valid-Accuracy : 0.9005768578215134
    Epoch - 76 Train-Loss : 1.0943613596584485 Train-Accuracy : 0.948993471164309
    Epoch - 76 Valid-Loss : 1.1426231656023251 Valid-Accuracy : 0.9005768578215134
    Epoch - 77 Train-Loss : 1.0944437006245489 Train-Accuracy : 0.948993471164309
    Epoch - 77 Valid-Loss : 1.1458655390688168 Valid-Accuracy : 0.9005768578215134
    Epoch - 78 Train-Loss : 1.0943450642668682 Train-Accuracy : 0.948993471164309
    Epoch - 78 Valid-Loss : 1.1426121996295067 Valid-Accuracy : 0.9005768578215134
    Epoch - 79 Train-Loss : 1.0944260721621306 Train-Accuracy : 0.948993471164309
    Epoch - 79 Valid-Loss : 1.142606803165969 Valid-Accuracy : 0.9005768578215134
    Changed learning rate to 2e-16
    Epoch - 80 Train-Loss : 1.0944223346917525 Train-Accuracy : 0.948993471164309
    Epoch - 80 Valid-Loss : 1.1458551063332507 Valid-Accuracy : 0.9005768578215134
    
    


```python
!python3 main.py --epochs 80 --model simpleSDT
```


```python
!python3 main.py --epochs 160 --model simpleLSTM --learning_rate 0.0001
```
